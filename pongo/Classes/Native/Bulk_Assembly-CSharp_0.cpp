﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Ball
struct Ball_t2062879;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;
// System.Object
struct Object_t;
// UnityEngine.Collision
struct Collision_t1119538015;
// Ball_Vertical
struct Ball_Vertical_t3666507190;
// GameController
struct GameController_t2782302542;
// Palas
struct Palas_t76878317;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_Ball2062879.h"
#include "AssemblyU2DCSharp_Ball2062879MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"
#include "UnityEngine_UnityEngine_Collision1119538015MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider955670625MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "AssemblyU2DCSharp_Ball_Vertical3666507190.h"
#include "AssemblyU2DCSharp_Ball_Vertical3666507190MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884.h"
#include "UnityEngine_UnityEngine_TouchPhase1905076713.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_GameController2782302542.h"
#include "AssemblyU2DCSharp_GameController2782302542MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharp_Palas76878317.h"
#include "AssemblyU2DCSharp_Palas76878317MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Object_t * Component_GetComponent_TisObject_t_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m267839954(__this, method) ((  Object_t * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, method) ((  Rigidbody_t1972007546 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Ball::.ctor()
extern "C"  void Ball__ctor_m1158546268 (Ball_t2062879 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ball::Awake()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3583;
extern Il2CppCodeGenString* _stringLiteral3584;
extern Il2CppCodeGenString* _stringLiteral3242;
extern Il2CppCodeGenString* _stringLiteral3243;
extern const uint32_t Ball_Awake_m1396151487_MetadataUsageId;
extern "C"  void Ball_Awake_m1396151487 (Ball_t2062879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ball_Awake_m1396151487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3583, /*hidden argument*/NULL);
		__this->___r1_3 = L_0;
		GameObject_t4012695102 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3584, /*hidden argument*/NULL);
		__this->___r2_4 = L_1;
		GameObject_t4012695102 * L_2 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3242, /*hidden argument*/NULL);
		__this->___r1_3 = L_2;
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3243, /*hidden argument*/NULL);
		__this->___r2_4 = L_3;
		Rigidbody_t1972007546 * L_4 = Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var);
		__this->___rb_7 = L_4;
		return;
	}
}
// System.Void Ball::Start()
extern "C"  void Ball_Start_m105684060 (Ball_t2062879 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)9), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) < ((int32_t)4)))
		{
			goto IL_0035;
		}
	}
	{
		Rigidbody_t1972007546 * L_2 = (__this->___rb_7);
		Vector2_t3525329788  L_3 = Vector2_get_right_m3495203638(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = (__this->___speed_2);
		Vector2_t3525329788  L_5 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_set_velocity_m799562119(L_2, L_6, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_0035:
	{
		Rigidbody_t1972007546 * L_7 = (__this->___rb_7);
		Vector2_t3525329788  L_8 = Vector2_get_left_m4093678863(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = (__this->___speed_2);
		Vector2_t3525329788  L_10 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t3525329789  L_11 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		Rigidbody_set_velocity_m799562119(L_7, L_11, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void Ball::OnCollisionEnter(UnityEngine.Collision)
extern TypeInfo* Single_t958209021_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral555895893;
extern Il2CppCodeGenString* _stringLiteral121;
extern const uint32_t Ball_OnCollisionEnter_m296899882_MetadataUsageId;
extern "C"  void Ball_OnCollisionEnter_m296899882 (Ball_t2062879 * __this, Collision_t1119538015 * ___col, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ball_OnCollisionEnter_m296899882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t3525329788  V_1 = {0};
	float V_2 = 0.0f;
	Vector2_t3525329788  V_3 = {0};
	float V_4 = 0.0f;
	Vector2_t3525329788  V_5 = {0};
	Bounds_t3518514978  V_6 = {0};
	Vector3_t3525329789  V_7 = {0};
	Vector2_t3525329788  V_8 = {0};
	Bounds_t3518514978  V_9 = {0};
	Vector3_t3525329789  V_10 = {0};
	Vector2_t3525329788  V_11 = {0};
	Bounds_t3518514978  V_12 = {0};
	Vector3_t3525329789  V_13 = {0};
	Vector2_t3525329788  V_14 = {0};
	Vector3_t3525329789  V_15 = {0};
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral555895893, /*hidden argument*/NULL);
		Collision_t1119538015 * L_0 = ___col;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Collision_get_gameObject_m4245316464(L_0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = (__this->___r1_3);
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00c1;
		}
	}
	{
		Transform_t284553113 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Vector2_t3525329788  L_6 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Collision_t1119538015 * L_7 = ___col;
		NullCheck(L_7);
		Transform_t284553113 * L_8 = Collision_get_transform_m3247945030(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3525329789  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		Vector2_t3525329788  L_10 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Collision_t1119538015 * L_11 = ___col;
		NullCheck(L_11);
		Collider_t955670625 * L_12 = Collision_get_collider_m1325344374(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Bounds_t3518514978  L_13 = Collider_get_bounds_m1050008332(L_12, /*hidden argument*/NULL);
		V_6 = L_13;
		Vector3_t3525329789  L_14 = Bounds_get_size_m3666348432((&V_6), /*hidden argument*/NULL);
		V_7 = L_14;
		float L_15 = ((&V_7)->___y_2);
		float L_16 = Ball_hitFactor_m2938689923(__this, L_6, L_10, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		float L_17 = V_0;
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral121, L_19, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		float L_21 = V_0;
		Vector2__ctor_m1517109030((&V_8), (1.0f), L_21, /*hidden argument*/NULL);
		Vector2_t3525329788  L_22 = Vector2_get_normalized_m123128511((&V_8), /*hidden argument*/NULL);
		V_1 = L_22;
		Rigidbody_t1972007546 * L_23 = Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var);
		Vector2_t3525329788  L_24 = V_1;
		float L_25 = (__this->___speed_2);
		Vector2_t3525329788  L_26 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		Vector3_t3525329789  L_27 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		NullCheck(L_23);
		Rigidbody_set_velocity_m799562119(L_23, L_27, /*hidden argument*/NULL);
		float L_28 = (__this->___speed_2);
		__this->___speed_2 = ((float)((float)L_28+(float)(0.1f)));
		goto IL_0238;
	}

IL_00c1:
	{
		Collision_t1119538015 * L_29 = ___col;
		NullCheck(L_29);
		GameObject_t4012695102 * L_30 = Collision_get_gameObject_m4245316464(L_29, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_31 = (__this->___r2_4);
		bool L_32 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0163;
		}
	}
	{
		Transform_t284553113 * L_33 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t3525329789  L_34 = Transform_get_position_m2211398607(L_33, /*hidden argument*/NULL);
		Vector2_t3525329788  L_35 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		Collision_t1119538015 * L_36 = ___col;
		NullCheck(L_36);
		Transform_t284553113 * L_37 = Collision_get_transform_m3247945030(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t3525329789  L_38 = Transform_get_position_m2211398607(L_37, /*hidden argument*/NULL);
		Vector2_t3525329788  L_39 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		Collision_t1119538015 * L_40 = ___col;
		NullCheck(L_40);
		Collider_t955670625 * L_41 = Collision_get_collider_m1325344374(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Bounds_t3518514978  L_42 = Collider_get_bounds_m1050008332(L_41, /*hidden argument*/NULL);
		V_9 = L_42;
		Vector3_t3525329789  L_43 = Bounds_get_size_m3666348432((&V_9), /*hidden argument*/NULL);
		V_10 = L_43;
		float L_44 = ((&V_10)->___y_2);
		float L_45 = Ball_hitFactor_m2938689923(__this, L_35, L_39, L_44, /*hidden argument*/NULL);
		V_2 = L_45;
		float L_46 = V_2;
		Vector2__ctor_m1517109030((&V_11), (-1.0f), L_46, /*hidden argument*/NULL);
		Vector2_t3525329788  L_47 = Vector2_get_normalized_m123128511((&V_11), /*hidden argument*/NULL);
		V_3 = L_47;
		Rigidbody_t1972007546 * L_48 = Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var);
		Vector2_t3525329788  L_49 = V_3;
		float L_50 = (__this->___speed_2);
		Vector2_t3525329788  L_51 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_49, L_50, /*hidden argument*/NULL);
		Vector3_t3525329789  L_52 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		NullCheck(L_48);
		Rigidbody_set_velocity_m799562119(L_48, L_52, /*hidden argument*/NULL);
		float L_53 = (__this->___speed_2);
		__this->___speed_2 = ((float)((float)L_53+(float)(0.1f)));
		goto IL_0238;
	}

IL_0163:
	{
		Collision_t1119538015 * L_54 = ___col;
		NullCheck(L_54);
		GameObject_t4012695102 * L_55 = Collision_get_gameObject_m4245316464(L_54, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_56 = (__this->___g1_5);
		bool L_57 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_017e;
		}
	}
	{
		goto IL_0238;
	}

IL_017e:
	{
		Collision_t1119538015 * L_58 = ___col;
		NullCheck(L_58);
		GameObject_t4012695102 * L_59 = Collision_get_gameObject_m4245316464(L_58, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_60 = (__this->___g2_6);
		bool L_61 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_0199;
		}
	}
	{
		goto IL_0238;
	}

IL_0199:
	{
		Transform_t284553113 * L_62 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		Vector3_t3525329789  L_63 = Transform_get_position_m2211398607(L_62, /*hidden argument*/NULL);
		Vector2_t3525329788  L_64 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		Collision_t1119538015 * L_65 = ___col;
		NullCheck(L_65);
		Transform_t284553113 * L_66 = Collision_get_transform_m3247945030(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		Vector3_t3525329789  L_67 = Transform_get_position_m2211398607(L_66, /*hidden argument*/NULL);
		Vector2_t3525329788  L_68 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		Collision_t1119538015 * L_69 = ___col;
		NullCheck(L_69);
		Collider_t955670625 * L_70 = Collision_get_collider_m1325344374(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		Bounds_t3518514978  L_71 = Collider_get_bounds_m1050008332(L_70, /*hidden argument*/NULL);
		V_12 = L_71;
		Vector3_t3525329789  L_72 = Bounds_get_size_m3666348432((&V_12), /*hidden argument*/NULL);
		V_13 = L_72;
		float L_73 = ((&V_13)->___y_2);
		float L_74 = Ball_hitFactor_m2938689923(__this, L_64, L_68, L_73, /*hidden argument*/NULL);
		V_4 = L_74;
		Rigidbody_t1972007546 * L_75 = Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var);
		NullCheck(L_75);
		Vector3_t3525329789  L_76 = Rigidbody_get_velocity_m2696244068(L_75, /*hidden argument*/NULL);
		V_15 = L_76;
		float L_77 = ((&V_15)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_78 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		float L_79 = V_4;
		Vector2__ctor_m1517109030((&V_14), L_78, L_79, /*hidden argument*/NULL);
		Vector2_t3525329788  L_80 = Vector2_get_normalized_m123128511((&V_14), /*hidden argument*/NULL);
		V_5 = L_80;
		Rigidbody_t1972007546 * L_81 = Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var);
		Vector2_t3525329788  L_82 = V_5;
		float L_83 = (__this->___speed_2);
		Vector2_t3525329788  L_84 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
		Vector3_t3525329789  L_85 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		NullCheck(L_81);
		Rigidbody_set_velocity_m799562119(L_81, L_85, /*hidden argument*/NULL);
		float L_86 = (__this->___speed_2);
		__this->___speed_2 = ((float)((float)L_86+(float)(0.1f)));
	}

IL_0238:
	{
		return;
	}
}
// System.Single Ball::hitFactor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  float Ball_hitFactor_m2938689923 (Ball_t2062879 * __this, Vector2_t3525329788  ___ballPos, Vector2_t3525329788  ___racketPos, float ___racketHeight, const MethodInfo* method)
{
	{
		float L_0 = ((&___ballPos)->___y_2);
		float L_1 = ((&___racketPos)->___y_2);
		float L_2 = ___racketHeight;
		return ((float)((float)((float)((float)L_0-(float)L_1))/(float)L_2));
	}
}
// System.Void Ball_Vertical::.ctor()
extern "C"  void Ball_Vertical__ctor_m2220488821 (Ball_Vertical_t3666507190 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ball_Vertical::Awake()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3583;
extern Il2CppCodeGenString* _stringLiteral3584;
extern Il2CppCodeGenString* _stringLiteral3585;
extern Il2CppCodeGenString* _stringLiteral3586;
extern Il2CppCodeGenString* _stringLiteral2479942;
extern const uint32_t Ball_Vertical_Awake_m2458094040_MetadataUsageId;
extern "C"  void Ball_Vertical_Awake_m2458094040 (Ball_Vertical_t3666507190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ball_Vertical_Awake_m2458094040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3583, /*hidden argument*/NULL);
		__this->___r1_3 = L_0;
		GameObject_t4012695102 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3584, /*hidden argument*/NULL);
		__this->___r2_4 = L_1;
		GameObject_t4012695102 * L_2 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3585, /*hidden argument*/NULL);
		__this->___r3_5 = L_2;
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3586, /*hidden argument*/NULL);
		__this->___r4_6 = L_3;
		GameObject_t4012695102 * L_4 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2479942, /*hidden argument*/NULL);
		__this->___pala_7 = L_4;
		Rigidbody_t1972007546 * L_5 = Component_GetComponent_TisRigidbody_t1972007546_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2174365699_MethodInfo_var);
		__this->___myRigidbody_9 = L_5;
		return;
	}
}
// System.Void Ball_Vertical::Start()
extern "C"  void Ball_Vertical_Start_m1167626613 (Ball_Vertical_t3666507190 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)9), /*hidden argument*/NULL);
		V_0 = L_0;
		return;
	}
}
// System.Void Ball_Vertical::FixedUpdate()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3002052;
extern const uint32_t Ball_Vertical_FixedUpdate_m709028400_MetadataUsageId;
extern "C"  void Ball_Vertical_FixedUpdate_m709028400 (Ball_Vertical_t3666507190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ball_Vertical_FixedUpdate_m709028400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0 = {0};
	Vector2_t3525329788  V_1 = {0};
	Touch_t1603883884  V_2 = {0};
	Touch_t1603883884  V_3 = {0};
	Touch_t1603883884  V_4 = {0};
	Touch_t1603883884  V_5 = {0};
	Touch_t1603883884  V_6 = {0};
	Vector3_t3525329789  V_7 = {0};
	Vector3_t3525329789  V_8 = {0};
	{
		Rigidbody_t1972007546 * L_0 = (__this->___myRigidbody_9);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Rigidbody_get_velocity_m2696244068(L_0, /*hidden argument*/NULL);
		__this->___oldVel_10 = L_1;
		bool L_2 = (__this->___isMoving_13);
		if (L_2)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_4 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Touch_get_phase_m3314549414((&V_2), /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_6 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_3 = L_6;
		Vector2_t3525329788  L_7 = Touch_get_deltaPosition_m3983677995((&V_3), /*hidden argument*/NULL);
		__this->___anchor_11 = L_7;
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_8 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_9 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_4 = L_9;
		int32_t L_10 = Touch_get_phase_m3314549414((&V_4), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_11 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_5 = L_11;
		Vector2_t3525329788  L_12 = Touch_get_deltaPosition_m3983677995((&V_5), /*hidden argument*/NULL);
		V_1 = L_12;
		Camera_t3533968274 * L_13 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t3525329788  L_14 = V_1;
		Vector3_t3525329789  L_15 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3525329789  L_16 = Camera_ScreenToWorldPoint_m1572306334(L_13, L_15, /*hidden argument*/NULL);
		Vector2_t3525329788  L_17 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		Vector2_t3525329788  L_18 = V_1;
		Vector2_t3525329788  L_19 = (__this->___anchor_11);
		Vector2_t3525329788  L_20 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		__this->___catapult_12 = L_20;
	}

IL_00a6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_21 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_22 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = Touch_get_phase_m3314549414((&V_6), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)3))))
		{
			goto IL_00e1;
		}
	}
	{
		Rigidbody_t1972007546 * L_24 = (__this->___myRigidbody_9);
		Vector2_t3525329788  L_25 = (__this->___catapult_12);
		Vector2_t3525329788  L_26 = Vector2_op_UnaryNegation_m1730705317(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Vector3_t3525329789  L_27 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		Rigidbody_set_velocity_m799562119(L_24, L_27, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		bool L_28 = (__this->___isMoving_13);
		if (!L_28)
		{
			goto IL_0144;
		}
	}
	{
		Transform_t284553113 * L_29 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t3525329789  L_30 = Transform_get_position_m2211398607(L_29, /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31 = ((&V_7)->___y_2);
		GameObject_t4012695102 * L_32 = (__this->___pala_7);
		NullCheck(L_32);
		Transform_t284553113 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t3525329789  L_34 = Transform_get_position_m2211398607(L_33, /*hidden argument*/NULL);
		V_8 = L_34;
		float L_35 = ((&V_8)->___y_2);
		if ((!(((float)L_31) < ((float)L_35))))
		{
			goto IL_0144;
		}
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral3002052, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_36 = (__this->___myRigidbody_9);
		Vector2_t3525329788  L_37 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_38 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Rigidbody_set_velocity_m799562119(L_36, L_38, /*hidden argument*/NULL);
		__this->___isMoving_13 = (bool)0;
	}

IL_0144:
	{
		return;
	}
}
// System.Void Ball_Vertical::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Ball_Vertical_OnCollisionEnter_m637249091 (Ball_Vertical_t3666507190 * __this, Collision_t1119538015 * ___c, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector2_t3525329788  V_1 = {0};
	ContactPoint_t2951122365  V_2 = {0};
	Bounds_t3518514978  V_3 = {0};
	Vector3_t3525329789  V_4 = {0};
	Vector2_t3525329788  V_5 = {0};
	{
		__this->___isMoving_13 = (bool)1;
		Collision_t1119538015 * L_0 = ___c;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Collision_get_gameObject_m4245316464(L_0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = (__this->___pala_7);
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0096;
		}
	}
	{
		Transform_t284553113 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Vector2_t3525329788  L_6 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Collision_t1119538015 * L_7 = ___c;
		NullCheck(L_7);
		Transform_t284553113 * L_8 = Collision_get_transform_m3247945030(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3525329789  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		Vector2_t3525329788  L_10 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Collision_t1119538015 * L_11 = ___c;
		NullCheck(L_11);
		Collider_t955670625 * L_12 = Collision_get_collider_m1325344374(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Bounds_t3518514978  L_13 = Collider_get_bounds_m1050008332(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		Vector3_t3525329789  L_14 = Bounds_get_size_m3666348432((&V_3), /*hidden argument*/NULL);
		V_4 = L_14;
		float L_15 = ((&V_4)->___x_1);
		float L_16 = Ball_Vertical_hitFactorx_m3063897210(__this, L_6, L_10, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		float L_17 = V_0;
		Vector2__ctor_m1517109030((&V_5), L_17, (1.0f), /*hidden argument*/NULL);
		Vector2_t3525329788  L_18 = Vector2_get_normalized_m123128511((&V_5), /*hidden argument*/NULL);
		V_1 = L_18;
		Rigidbody_t1972007546 * L_19 = (__this->___myRigidbody_9);
		Vector2_t3525329788  L_20 = V_1;
		float L_21 = (__this->___speed_2);
		Vector2_t3525329788  L_22 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Vector3_t3525329789  L_23 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		NullCheck(L_19);
		Rigidbody_set_velocity_m799562119(L_19, L_23, /*hidden argument*/NULL);
		goto IL_010f;
	}

IL_0096:
	{
		Collision_t1119538015 * L_24 = ___c;
		NullCheck(L_24);
		ContactPointU5BU5D_t1988025008* L_25 = Collision_get_contacts_m658316947(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		V_2 = (*(ContactPoint_t2951122365 *)((ContactPoint_t2951122365 *)(ContactPoint_t2951122365 *)SZArrayLdElema(L_25, 0, sizeof(ContactPoint_t2951122365 ))));
		Rigidbody_t1972007546 * L_26 = (__this->___myRigidbody_9);
		Vector3_t3525329789  L_27 = (__this->___oldVel_10);
		Vector3_t3525329789  L_28 = ContactPoint_get_normal_m1137164497((&V_2), /*hidden argument*/NULL);
		Vector3_t3525329789 * L_29 = &(__this->___oldVel_10);
		float L_30 = Vector3_get_magnitude_m989985786(L_29, /*hidden argument*/NULL);
		Vector3_t3525329789  L_31 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		Vector3_t3525329789  L_32 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_27, L_31, /*hidden argument*/NULL);
		NullCheck(L_26);
		Rigidbody_set_velocity_m799562119(L_26, L_32, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_33 = (__this->___myRigidbody_9);
		Vector3_t3525329789  L_34 = (__this->___oldVel_10);
		Vector3_t3525329789  L_35 = ContactPoint_get_normal_m1137164497((&V_2), /*hidden argument*/NULL);
		Vector3_t3525329789  L_36 = Vector3_Reflect_m2873550862(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		Rigidbody_set_velocity_m799562119(L_33, L_36, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_37 = (__this->___myRigidbody_9);
		Rigidbody_t1972007546 * L_38 = L_37;
		NullCheck(L_38);
		Vector3_t3525329789  L_39 = Rigidbody_get_velocity_m2696244068(L_38, /*hidden argument*/NULL);
		Vector3_t3525329789  L_40 = ContactPoint_get_normal_m1137164497((&V_2), /*hidden argument*/NULL);
		Vector3_t3525329789  L_41 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		Rigidbody_set_velocity_m799562119(L_38, L_41, /*hidden argument*/NULL);
	}

IL_010f:
	{
		return;
	}
}
// System.Single Ball_Vertical::hitFactory(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  float Ball_Vertical_hitFactory_m3084234585 (Ball_Vertical_t3666507190 * __this, Vector2_t3525329788  ___ballPos, Vector2_t3525329788  ___racketPos, float ___racketHeight, const MethodInfo* method)
{
	{
		float L_0 = ((&___ballPos)->___y_2);
		float L_1 = ((&___racketPos)->___y_2);
		return ((float)((float)L_0-(float)L_1));
	}
}
// System.Single Ball_Vertical::hitFactorx(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  float Ball_Vertical_hitFactorx_m3063897210 (Ball_Vertical_t3666507190 * __this, Vector2_t3525329788  ___ballPos, Vector2_t3525329788  ___racketPos, float ___racketWidth, const MethodInfo* method)
{
	{
		float L_0 = ((&___ballPos)->___x_1);
		float L_1 = ((&___racketPos)->___x_1);
		float L_2 = ___racketWidth;
		return ((float)((float)((float)((float)L_0-(float)L_1))/(float)L_2));
	}
}
// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m4168274701 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Awake()
extern "C"  void GameController_Awake_m110912624 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	{
		Application_set_targetFrameRate_m498658007(NULL /*static, unused*/, ((int32_t)90), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Start()
extern "C"  void GameController_Start_m3115412493 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	{
		GameController_BeginGame_m97397926(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Update()
extern "C"  void GameController_Update_m2094358944 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameController::BeginGame()
extern TypeInfo* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const uint32_t GameController_BeginGame_m97397926_MetadataUsageId;
extern "C"  void GameController_BeginGame_m97397926 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameController_BeginGame_m97397926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = {0};
	{
		GameObject_t4012695102 * L_0 = (__this->___ballPrefab_4);
		Vector2_t3525329788  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3878351788 * L_4 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)IsInstSealed(L_4, GameObject_t4012695102_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Palas::.ctor()
extern "C"  void Palas__ctor_m2206306846 (Palas_t76878317 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Palas::Start()
extern "C"  void Palas_Start_m1153444638 (Palas_t76878317 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t284553113 * L_1 = Transform_GetChild_m4040462992(L_0, 0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t4012695102 * L_2 = Component_get_gameObject_m1170635899(L_1, /*hidden argument*/NULL);
		__this->___pala1_2 = L_2;
		Transform_t284553113 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t284553113 * L_4 = Transform_GetChild_m4040462992(L_3, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		__this->___pala2_3 = L_5;
		return;
	}
}
// System.Void Palas::FixedUpdate()
extern TypeInfo* Input_t1593691127_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t Palas_FixedUpdate_m1972429081_MetadataUsageId;
extern "C"  void Palas_FixedUpdate_m1972429081 (Palas_t76878317 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Palas_FixedUpdate_m1972429081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0 = {0};
	Touch_t1603883884  V_1 = {0};
	Touch_t1603883884  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t4012695102 * L_1 = (__this->___pala1_2);
		NullCheck(L_1);
		Transform_t284553113 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		Vector2_t3525329788  L_3 = Vector2_get_up_m1197831267(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = (__this->___speed_4);
		Vector2_t3525329788  L_5 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_Translate_m2849099360(L_2, L_6, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = (__this->___pala2_3);
		NullCheck(L_7);
		Transform_t284553113 * L_8 = GameObject_get_transform_m1278640159(L_7, /*hidden argument*/NULL);
		Vector2_t3525329788  L_9 = Vector2_get_down_m3874381546(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = (__this->___speed_4);
		Vector2_t3525329788  L_11 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector3_t3525329789  L_12 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_Translate_m2849099360(L_8, L_12, /*hidden argument*/NULL);
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_13 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)115), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00ac;
		}
	}
	{
		GameObject_t4012695102 * L_14 = (__this->___pala1_2);
		NullCheck(L_14);
		Transform_t284553113 * L_15 = GameObject_get_transform_m1278640159(L_14, /*hidden argument*/NULL);
		Vector2_t3525329788  L_16 = Vector2_get_down_m3874381546(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = (__this->___speed_4);
		Vector2_t3525329788  L_18 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		Vector3_t3525329789  L_19 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_Translate_m2849099360(L_15, L_19, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_20 = (__this->___pala2_3);
		NullCheck(L_20);
		Transform_t284553113 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		Vector2_t3525329788  L_22 = Vector2_get_up_m1197831267(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = (__this->___speed_4);
		Vector2_t3525329788  L_24 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		Vector3_t3525329789  L_25 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_Translate_m2849099360(L_21, L_25, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_26 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_26) <= ((int32_t)0)))
		{
			goto IL_0189;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_27 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_27;
		int32_t L_28 = Touch_get_phase_m3314549414((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_28) == ((uint32_t)1))))
		{
			goto IL_0189;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_29 = Input_GetTouch_m2282421092(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_29;
		Vector2_t3525329788  L_30 = Touch_get_deltaPosition_m3983677995((&V_2), /*hidden argument*/NULL);
		V_0 = L_30;
		float L_31 = ((&V_0)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_32 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if ((!(((float)L_32) > ((float)(0.0f)))))
		{
			goto IL_013f;
		}
	}
	{
		GameObject_t4012695102 * L_33 = (__this->___pala1_2);
		NullCheck(L_33);
		Transform_t284553113 * L_34 = GameObject_get_transform_m1278640159(L_33, /*hidden argument*/NULL);
		Vector2_t3525329788  L_35 = Vector2_get_down_m3874381546(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_36 = (__this->___speed_4);
		Vector2_t3525329788  L_37 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t3525329789  L_38 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_Translate_m2849099360(L_34, L_38, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_39 = (__this->___pala2_3);
		NullCheck(L_39);
		Transform_t284553113 * L_40 = GameObject_get_transform_m1278640159(L_39, /*hidden argument*/NULL);
		Vector2_t3525329788  L_41 = Vector2_get_up_m1197831267(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_42 = (__this->___speed_4);
		Vector2_t3525329788  L_43 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t3525329789  L_44 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_Translate_m2849099360(L_40, L_44, /*hidden argument*/NULL);
		goto IL_0189;
	}

IL_013f:
	{
		GameObject_t4012695102 * L_45 = (__this->___pala1_2);
		NullCheck(L_45);
		Transform_t284553113 * L_46 = GameObject_get_transform_m1278640159(L_45, /*hidden argument*/NULL);
		Vector2_t3525329788  L_47 = Vector2_get_up_m1197831267(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_48 = (__this->___speed_4);
		Vector2_t3525329788  L_49 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		Vector3_t3525329789  L_50 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_Translate_m2849099360(L_46, L_50, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_51 = (__this->___pala2_3);
		NullCheck(L_51);
		Transform_t284553113 * L_52 = GameObject_get_transform_m1278640159(L_51, /*hidden argument*/NULL);
		Vector2_t3525329788  L_53 = Vector2_get_down_m3874381546(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_54 = (__this->___speed_4);
		Vector2_t3525329788  L_55 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		Vector3_t3525329789  L_56 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_Translate_m2849099360(L_52, L_56, /*hidden argument*/NULL);
	}

IL_0189:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
