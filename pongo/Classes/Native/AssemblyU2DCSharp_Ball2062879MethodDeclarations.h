﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ball
struct Ball_t2062879;
// UnityEngine.Collision
struct Collision_t1119538015;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void Ball::.ctor()
extern "C"  void Ball__ctor_m1158546268 (Ball_t2062879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball::Awake()
extern "C"  void Ball_Awake_m1396151487 (Ball_t2062879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball::Start()
extern "C"  void Ball_Start_m105684060 (Ball_t2062879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Ball_OnCollisionEnter_m296899882 (Ball_t2062879 * __this, Collision_t1119538015 * ___col, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Ball::hitFactor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  float Ball_hitFactor_m2938689923 (Ball_t2062879 * __this, Vector2_t3525329788  ___ballPos, Vector2_t3525329788  ___racketPos, float ___racketHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
