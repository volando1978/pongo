﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// Ball_Vertical
struct  Ball_Vertical_t3666507190  : public MonoBehaviour_t3012272455
{
	// System.Single Ball_Vertical::speed
	float ___speed_2;
	// UnityEngine.GameObject Ball_Vertical::r1
	GameObject_t4012695102 * ___r1_3;
	// UnityEngine.GameObject Ball_Vertical::r2
	GameObject_t4012695102 * ___r2_4;
	// UnityEngine.GameObject Ball_Vertical::r3
	GameObject_t4012695102 * ___r3_5;
	// UnityEngine.GameObject Ball_Vertical::r4
	GameObject_t4012695102 * ___r4_6;
	// UnityEngine.GameObject Ball_Vertical::pala
	GameObject_t4012695102 * ___pala_7;
	// UnityEngine.GameObject Ball_Vertical::raqueta
	GameObject_t4012695102 * ___raqueta_8;
	// UnityEngine.Rigidbody Ball_Vertical::myRigidbody
	Rigidbody_t1972007546 * ___myRigidbody_9;
	// UnityEngine.Vector3 Ball_Vertical::oldVel
	Vector3_t3525329789  ___oldVel_10;
	// UnityEngine.Vector2 Ball_Vertical::anchor
	Vector2_t3525329788  ___anchor_11;
	// UnityEngine.Vector2 Ball_Vertical::catapult
	Vector2_t3525329788  ___catapult_12;
	// System.Boolean Ball_Vertical::isMoving
	bool ___isMoving_13;
};
