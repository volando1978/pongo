﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Palas
struct Palas_t76878317;

#include "codegen/il2cpp-codegen.h"

// System.Void Palas::.ctor()
extern "C"  void Palas__ctor_m2206306846 (Palas_t76878317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Palas::Start()
extern "C"  void Palas_Start_m1153444638 (Palas_t76878317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Palas::FixedUpdate()
extern "C"  void Palas_FixedUpdate_m1972429081 (Palas_t76878317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
