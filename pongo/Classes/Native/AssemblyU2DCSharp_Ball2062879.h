﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// Ball
struct  Ball_t2062879  : public MonoBehaviour_t3012272455
{
	// System.Single Ball::speed
	float ___speed_2;
	// UnityEngine.GameObject Ball::r1
	GameObject_t4012695102 * ___r1_3;
	// UnityEngine.GameObject Ball::r2
	GameObject_t4012695102 * ___r2_4;
	// UnityEngine.GameObject Ball::g1
	GameObject_t4012695102 * ___g1_5;
	// UnityEngine.GameObject Ball::g2
	GameObject_t4012695102 * ___g2_6;
	// UnityEngine.Rigidbody Ball::rb
	Rigidbody_t1972007546 * ___rb_7;
};
