﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// GameController
struct  GameController_t2782302542  : public MonoBehaviour_t3012272455
{
	// UnityEngine.GameObject GameController::goalL
	GameObject_t4012695102 * ___goalL_2;
	// UnityEngine.GameObject GameController::goalR
	GameObject_t4012695102 * ___goalR_3;
	// UnityEngine.GameObject GameController::ballPrefab
	GameObject_t4012695102 * ___ballPrefab_4;
	// UnityEngine.UI.Text GameController::score_R
	Text_t3286458198 * ___score_R_5;
	// UnityEngine.UI.Text GameController::score_L
	Text_t3286458198 * ___score_L_6;
	// System.Int32 GameController::score1
	int32_t ___score1_7;
	// System.Int32 GameController::score2
	int32_t ___score2_8;
};
