﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// Palas
struct  Palas_t76878317  : public MonoBehaviour_t3012272455
{
	// UnityEngine.GameObject Palas::pala1
	GameObject_t4012695102 * ___pala1_2;
	// UnityEngine.GameObject Palas::pala2
	GameObject_t4012695102 * ___pala2_3;
	// System.Single Palas::speed
	float ___speed_4;
};
