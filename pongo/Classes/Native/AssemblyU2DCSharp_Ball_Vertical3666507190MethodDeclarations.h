﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ball_Vertical
struct Ball_Vertical_t3666507190;
// UnityEngine.Collision
struct Collision_t1119538015;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void Ball_Vertical::.ctor()
extern "C"  void Ball_Vertical__ctor_m2220488821 (Ball_Vertical_t3666507190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball_Vertical::Awake()
extern "C"  void Ball_Vertical_Awake_m2458094040 (Ball_Vertical_t3666507190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball_Vertical::Start()
extern "C"  void Ball_Vertical_Start_m1167626613 (Ball_Vertical_t3666507190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball_Vertical::FixedUpdate()
extern "C"  void Ball_Vertical_FixedUpdate_m709028400 (Ball_Vertical_t3666507190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball_Vertical::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Ball_Vertical_OnCollisionEnter_m637249091 (Ball_Vertical_t3666507190 * __this, Collision_t1119538015 * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Ball_Vertical::hitFactory(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  float Ball_Vertical_hitFactory_m3084234585 (Ball_Vertical_t3666507190 * __this, Vector2_t3525329788  ___ballPos, Vector2_t3525329788  ___racketPos, float ___racketHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Ball_Vertical::hitFactorx(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  float Ball_Vertical_hitFactorx_m3063897210 (Ball_Vertical_t3666507190 * __this, Vector2_t3525329788  ___ballPos, Vector2_t3525329788  ___racketPos, float ___racketWidth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
