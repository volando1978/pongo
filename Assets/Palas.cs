﻿using UnityEngine;
using System.Collections;

public class Palas : MonoBehaviour
{


	GameObject pala1;
	GameObject pala2;

	public float speed;

	// Use this for initialization
	void Start ()
	{
		pala1 = transform.GetChild (0).gameObject;
		pala2 = transform.GetChild (1).gameObject;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (Input.GetKey (KeyCode.A)) {
			pala1.transform.Translate (Vector2.up * speed);
			pala2.transform.Translate (Vector2.down * speed);
		}

		if (Input.GetKey (KeyCode.S)) {
			pala1.transform.Translate (Vector2.down * speed);
			pala2.transform.Translate (Vector2.up * speed);
		}
	

		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {
				
			Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;

			if (Mathf.Sign (touchDeltaPosition.x) > 0) {
				pala1.transform.Translate (Vector2.down * speed);
				pala2.transform.Translate (Vector2.up * speed);
			} else {
				pala1.transform.Translate (Vector2.up * speed);
				pala2.transform.Translate (Vector2.down * speed);
			}
		}


	}

}
