﻿using UnityEngine;
using System.Collections;

public class Ball_Vertical : MonoBehaviour
{

	public float speed;
	public GameObject r1;
	public GameObject r2;
	public GameObject r3;
	public GameObject r4;
	public GameObject pala;


	public GameObject raqueta;

	Rigidbody myRigidbody;

	Vector3 oldVel;
	Vector2 anchor;
	Vector2 catapult;
	public bool isMoving = false;

	void Awake ()
	{
		r1 = GameObject.Find ("r1");
		r2 = GameObject.Find ("r2");
		r3 = GameObject.Find ("r3");
		r4 = GameObject.Find ("r4");
		pala = GameObject.Find ("Pala");
		myRigidbody = GetComponent<Rigidbody> ();
	}
	// Use this for initialization
	void Start ()
	{

		int r = Random.Range (0, 9);

//		if (r >= 4) {
//			myRigidbody.velocity = Vector2.up * speed;
//		} else {
//			myRigidbody.velocity = Vector2.down * speed;
//		}

	}

	void FixedUpdate ()
	{
		oldVel = myRigidbody.velocity;


		if (!isMoving && Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
			anchor = Input.GetTouch (0).deltaPosition;

		}

		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {

			Vector2 dir;
			Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;

			dir = Camera.main.ScreenToWorldPoint (touchDeltaPosition);
			catapult = touchDeltaPosition - anchor;

		}


		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Ended) {
			myRigidbody.velocity = -catapult;


		}

		if (isMoving && transform.position.y < pala.transform.position.y) {
			print ("aqui");

			myRigidbody.velocity = Vector2.zero;
			isMoving = false;
		}

	}

	// Update is called once per frame
	void OnCollisionEnter (Collision c)
	{
		isMoving = true;
		if (c.gameObject == pala) {

			// Calculate hit Factor
			float x = hitFactorx (transform.position,
				          c.transform.position,
				          c.collider.bounds.size.x);

			// Calculate direction, set length to 1
			Vector2 dir = new Vector2 (x, 1).normalized;

			// Set Velocity with dir * speed
			myRigidbody.velocity = dir * speed;
		} else {

//		var hit = col.contacts [0];
//		Debug.DrawRay (hit.point, -GetComponent<Rigidbody> ().velocity, Color.green, 0.6f); // draw red "in" velocity
//		print (gameObject.GetComponent<Rigidbody> ().velocity);
//		Vector3 dir = Vector3.Reflect (transform.position, hit.normal); // reflect the velocity
//
//		GetComponent<Rigidbody> ().velocity = dir * speed / 2;
//
//		Debug.DrawRay (hit.point, gameObject.GetComponent<Rigidbody> ().velocity, Color.blue, 0.6f); // draw blue "out" velocity
			ContactPoint cp = c.contacts [0];
			// calculate with addition of normal vector
			myRigidbody.velocity = oldVel + cp.normal * oldVel.magnitude;

			// calculate with Vector3.Reflect
			myRigidbody.velocity = Vector3.Reflect (oldVel, cp.normal);

			// bumper effect to speed up ball
			myRigidbody.velocity += cp.normal;
		}



	}


	float hitFactory (Vector2 ballPos, Vector2 racketPos, float racketHeight)
	{
		return (ballPos.y - racketPos.y);
	}

	float hitFactorx (Vector2 ballPos, Vector2 racketPos, float racketWidth)
	{
		// ascii art:
		//
		// 1  -0.5  0  0.5   1  <- x value
		// ===================  <- racket
		//
		return (ballPos.x - racketPos.x) / racketWidth;
	}


}

