﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
	public float speed;
	public GameObject r1;
	public GameObject r2;

	public GameObject g1;
	public GameObject g2;

	Rigidbody rb;



	void Awake ()
	{
		r1 = GameObject.Find ("r1");
		r2 = GameObject.Find ("r2");

		r1 = GameObject.Find ("g1");
		r2 = GameObject.Find ("g2");
		rb = GetComponent<Rigidbody> ();

	}
	// Use this for initialization
	void Start ()
	{

		int r = Random.Range (0, 9);

		if (r >= 4) {
			rb.velocity = Vector2.right * speed;
		} else {
			rb.velocity = Vector2.left * speed;
		}




	}
	
	// Update is called once per frame
	void OnCollisionEnter (Collision col)
	{
		print ("Im working");
		if (col.gameObject == r1) {//left racket
			// Calculate hit Factor
			float y = hitFactor (transform.position,
				          col.transform.position,
				          col.collider.bounds.size.y);
			print ("y" + y);

			Vector2 dir = new Vector2 (1, y).normalized;
	
			GetComponent<Rigidbody> ().velocity = dir * speed;
			speed += 0.1f;


		} else if (col.gameObject == r2) { //right racket
			// Calculate hit Factor
			float y = hitFactor (transform.position,
				          col.transform.position,
				          col.collider.bounds.size.y);

			Vector2 dir = new Vector2 (-1, y).normalized;

			// Set Velocity with dir * speed
			GetComponent<Rigidbody> ().velocity = dir * speed;
			speed += 0.1f;

		} else if (col.gameObject == g1) { //right racket
			

		} else if (col.gameObject == g2) { //right racket


		} else {
			float y = hitFactor (transform.position,
				          col.transform.position,
				          col.collider.bounds.size.y);
			Vector2 dir = new Vector2 (Mathf.Sign (GetComponent<Rigidbody> ().velocity.x), y).normalized;

			// Set Velocity with dir * speed
			GetComponent<Rigidbody> ().velocity = dir * speed;
			speed += 0.1f;
		}


	
	}


	float hitFactor (Vector2 ballPos, Vector2 racketPos, float racketHeight)
	{
		return (ballPos.y - racketPos.y) / racketHeight;
	}
}
